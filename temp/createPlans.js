var Plan = require('../models/plan').Plan;

var monthPlan = new Plan({
	name    : 'Monthly',
	type    : 'month',

	customQuote     : 0,
	leadGrowth      : 0,
	salesOpportunity: 0,
	salesBoost      : 0
});

var annualPlan = new Plan({
	name    : 'Annual',
	type    : 'annual',

	customQuote     : 0,
	leadGrowth      : 0,
	salesOpportunity: 0,
	salesBoost      : 0
});

monthPlan.save(function(err) {
	if (err) {
		console.error(err);
	} else {
		console.log('Month Done');

		annualPlan.save(function(err) {
			if (err) {
				console.error(err);
			} else {
				console.log('Annual Done');
			}
		});
	}
});