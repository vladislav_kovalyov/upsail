const mongoose = require('../config/mongoose');
const autoIncrement = require('../config/mongoose').autoIncrement;

const Schema = mongoose.Schema;

const types = 'month annual'.split(' ');

const planSchema = new Schema({
	plan_id    : {type: Number, unique: true},
	name       : {type: String, required: true},
	description: {type: String},
	type       : {type: String, enum: types, default: types[0]},

	customQuote     : {type: Number, required: true},
	leadGrowth      : {type: Number, required: true},
	salesOpportunity: {type: Number, required: true},
	salesBoost      : {type: Number, required: true}
});

planSchema.plugin(autoIncrement.plugin, {
	model      : 'Plan',
	field      : 'plan_id',
	startAt    : 1,
	incrementBy: 1
});

exports.Plan = mongoose.model('Plan', planSchema);
exports.types = types;