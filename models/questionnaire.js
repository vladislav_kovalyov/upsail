/*request on the site*/
var mongoose = require('../config/mongoose');
var autoIncrement = require('../config/mongoose').autoIncrement;

var Schema = mongoose.Schema;

var questionnaireSchema = new Schema({
	questionnaire_id : {type: Number, unique: true},

	firstName:  {type: String},
	lastName:  {type: String},

	company: {type: String},
	title  : {type: String},
	email  : {type: String},
	phone  : {type: String},
	txtar1 : {type: String},
	txtar2 : {type: String},
	txtar3 : {type: String},
	txtar4 : {type: String},
	txtar5 : {type: String},
	txtar6 : {type: String},
	txtar7 : {type: String},
	txtar8 : {type: String},
	txtar9 : {type: String},
	txtar10: {type: String},

	employee_size : {type: String},
	company_revenue: {type: String},

	package: {type: String},
	price  : {type: String},
	type   : {type: String},

	date   : {type: Date}
});

questionnaireSchema.plugin(autoIncrement.plugin, {
	model      : 'Questionnaire',
	field      : 'questionnaire_id',
	startAt    : 1,
	incrementBy: 1
});

exports.Questionnaire = mongoose.model('Questionnaire', questionnaireSchema);