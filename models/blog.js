var mongoose = require('../config/mongoose');
var autoIncrement = require('../config/mongoose').autoIncrement;

var Schema = mongoose.Schema;

var blogSchema = new Schema({
    blog_id : {type: Number, unique: true},

    title      : {type: String},
    author     : {type: String},
    picture    : {type: String},
    text       : {type: String},
    description: {type: String},

    dateFormatted : {type: String},

    views    : {type: Number, default: 0},
    priority : {type: Number, default: 1},

    created : {type: Date, default: Date.now()}
});

blogSchema.plugin(autoIncrement.plugin, {
    model      : 'Blog',
    field      : 'blog_id',
    startAt    : 1,
    incrementBy: 1
});

exports.Blog = mongoose.model('Blog', blogSchema);