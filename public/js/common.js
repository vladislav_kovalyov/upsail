

$(document).ready(function(){
	$('.dd_menu').hover(function(){
	$('.dropdown__hover').fadeIn(300);
		},function(){
	$('.dropdown__hover').fadeOut(300);})

	var type, href;
	
	$('.switcher').click(function(){
		$('.switcher__circle').toggleClass('run');
		
		if ($('.switcher__circle').hasClass('run'))
		{
			$('.switcher__txt1').removeClass('switcher__txt_color');
			$('.switcher__txt2').addClass('switcher__txt_color');
		} else {
			$('.switcher__txt2').removeClass('switcher__txt_color');
			$('.switcher__txt1').addClass('switcher__txt_color');
		}

		type = $('.switcher__txt_color').data('type');
		$('.package-block .btn').each(function(){
			$(this).attr('href', function(i,a){
				return a.replace( /(type=)[a-z]+/ig, "type=" + type );
			});
		});
	});
	
	let menu = $('.menu_responsive');
	
	$('.burger').click(function(){
		$(menu).addClass('active');
	})
	
	$('.close-btn').click(function(){
		$(menu).removeClass('active');
	})
	
	
})


// $('.checkbox-block').click(function(){
// 	if ($(this).parent().hasClass('form__checkbox-wrap1')) {
// 		$('.form__checkbox-wrap1 label').removeClass('active');
// 		//$(this).find('label').addClass('active');
// 	}
//
// 	if ($(this).parent().hasClass('form__checkbox-wrap2')) {
// 		$('.form__checkbox-wrap2 label').removeClass('active');
// 		//$(this).find('label').addClass('active');
// 	}
//
// 	$(this).find('label').addClass('active');
// });


$(document).ready(function(){
  $(".btn-anchor").on('click', function(event) {
    if (this.hash !== "") {
      event.preventDefault();
      var hash = this.hash;
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){
        window.location.hash = hash;
      });
    } 
  });
});

