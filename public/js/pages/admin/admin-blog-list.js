var table_all_global = null;

var $ = jQuery.noConflict();

$(document).ready(function() {

    var table = $('#table_blog').DataTable({
        ajax           : {
            url    : "/admin/blog/list",
            columns: [
                {data: 'blog_id'},
                {data: 'title'},
                //{data: 'priority'}
            ]
        },
        columnDefs: [
            {
                targets   : 1,
                searchable: true,
                orderable : true,
                className : 'dt-body-center',
                render    : function(data, type, full, meta) {
                    return '<a href="/admin/blog/' + full[0] + '">' + data + '</a>'
                }
            }
        ],
        order          : [0, 'desc'],
        select         : true,
        "fnRowCallback": function(nRow, aData, iDisplayIndex) {
            nRow.setAttribute('id', aData[0]);

            nRow.lastChild.innerHTML = '';

            var aTag = document.createElement('a');
            aTag.setAttribute('title', 'Remove');
            aTag.setAttribute('style', 'cursor:pointer');
            aTag.onclick = function() {
                deleteItem(aData[0]);
            };

            var iTag = document.createElement('i');
            iTag.setAttribute('class', 'fa fa-trash-o');
            aTag.appendChild(iTag);

            nRow.lastChild.appendChild(aTag);
        },
        aLengthMenu    : [
            [10, 25, 50, 100, 200, -1],
            [10, 25, 50, 100, 200, "All"]
        ],
        iDisplayLength : 50
    });

    $('#table_admins tbody').on('click', 'tr', function() {
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    });

    table_all_global = table;
});

function deleteItem(selectedId) {
    Dialog("Удалить пост?", function() {
        deleteItemBack(selectedId)
    }, function() {
        return false
    });
}

function deleteItemBack(selectedId) {
    if (!window.location.origin) {
        window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
    }

    $.get(window.location.origin + "/admin/blog/" + selectedId + "/delete", function(resp) {
        if (resp.result){
            table_all_global.ajax.reload();
        } else {
            if (resp.error){
                alert('Ошибка при удалении');
            }
        }
    });
}