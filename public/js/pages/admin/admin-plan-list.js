var table_all_global = null;

var $ = jQuery.noConflict();

$(document).ready(function() {

    var table = $('#table_plans').DataTable({
        ajax           : {
            url    : "/admin/plans/list",
        },
        order          : [0, 'asc'],
        select         : true,
        "fnRowCallback": function(nRow, aData, iDisplayIndex) {
            nRow.setAttribute('id', aData[0]);

			nRow.lastChild.innerHTML = '';

			var aTag = document.createElement('a');
			aTag.setAttribute('title', 'Edit');
			aTag.setAttribute('style', 'cursor:pointer; margin-right:5px');
			aTag.onclick = function() {
				editItem(aData[0]);
			};

			var iTag = document.createElement('i');
			iTag.setAttribute('class', 'fa fa-pencil');
			aTag.appendChild(iTag);

            nRow.lastChild.appendChild(aTag);
        },
        aLengthMenu    : [
            [10, 25, 50, 100, 200, -1],
            [10, 25, 50, 100, 200, "All"]
        ],
        iDisplayLength : 50
    });
});

function editItem(id) {
	window.location = '/admin/plans/' + id;
}
