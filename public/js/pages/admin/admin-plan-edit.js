var $ = jQuery.noConflict();

$(document).ready(function() {
	function isNumeric(n) {
		return !isNaN(parseFloat(n)) && isFinite(n);
	}

	$('.price').on('input', function(event) {
		if ($(this).val().charAt(0) === "0" && $(this).val().charAt(1) !== ".") {
			$(this).val($(this).val().substr(1));
		}
		if (!isNumeric($(this).val())){
			$(this).val('0');
		}
	}).on('keypress', function(e) {
		if ($.inArray(e.keyCode, [0, 8, 9, 27, 46, 13, 110, 190]) !== -1 ||
			(e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
			(e.keyCode >= 35 && e.keyCode <= 40)) {
			return;
		}
		if (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) {
			e.preventDefault();
		} else {
			if ($(this).val() === ''){
				$(this).val('0');
			}
		}
	}).on('change', function(e) {
		$(this).val((parseInt($(this).val() * 100)) / 100);
	});

	$('#plan-form').submit(function(e){
		var name = $('#name');
		if (name.val() === ''){
			e.preventDefault();
			setError('Set plan name');
			name.focus();
		}
	});
});
