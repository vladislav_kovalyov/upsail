fn = function(req, res) {
    var fs = require('fs');

    fs.readFile(req.files.file.path, function (err, data) {
        var date = new Date();
        var newPath = __dirname + '/public/uploads/' + date.getFullYear() + '_' + date.getMonth() + '_' + date.getDate() + '_' + req.files.file.name;
        newPath = newPath.split('\\').join('/');
        var sendPath = '/uploads/' + date.getFullYear() + '_' + date.getMonth() + '_' + date.getDate() + '_' + req.files.file.name;
        fs.writeFile(newPath, data, function (err) {
            if (err) {
                console.log(err);
                res.send({location: ''});
            } else {
                res.send({location: sendPath});
            }
        });
    });
};

exports.fn = fn;