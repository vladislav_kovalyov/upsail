var nodemailer = require('nodemailer');
var eLogger = require('../lib/logger').eLogger;

var Plan = require('../models/plan').Plan;
var Questionnaire = require('../models/questionnaire').Questionnaire;

var sendMessage = require('../lib/googleApiMessage').sendMessage;

module.exports = function (app, passport) {
    var bodyParser = require ('body-parser');
    app.use (bodyParser.urlencoded ({extended: true, keepExtensions: true}));

    app.get ('/admin', function (req, res, next) {
        if (req.isAuthenticated ()) {
            next();
        } else {
            res.redirect ('/login');
        }
    });

    app.get('/login', function (req, res) {
        res.render('pages/login', {
            message: req.flash('loginMessage')
        });
    });

    app.post("/login", passport.authenticate('local-login',
        {
            successRedirect: '/admin',
            failureRedirect: '/login',
            failureFlash   : true
        }), function (req, res) {

        if (req.body.rememberMe) {
            req.session.cookie.maxAge = 365 * 24 * 60 * 60 * 1000; // Cookie expires after 365 days
        } else {
            req.session.cookie.expires = false; // Cookie expires at end of session
        }
    });

    app.get('/logout', function (req, res) {
        req.session.destroy(req.session);
        req.logout();
        res.redirect('/');
    });

    app.get('/', function(req, res, next) {
        res.render('pages/index', {});
    });

    app.get('/plans-and-pricing', function(req, res, next) {
    	const data = {
    		monthly : {},
			annual  : {}
		};

		Plan.find({})
			.select('type customQuote leadGrowth salesOpportunity salesBoost')
			.lean()
			.exec(function (err, plans) {
				if (plans && !err) {
					plans.forEach(function(item){
						if (item.type === 'month'){
							data.monthly = item;
						} else if (item.type === 'annual'){
							data.annual = item;
						}
					});
					res.render('pages/plans-and-pricing', data);
				} else {
					res.render('pages/plans-and-pricing', data);
				}
			});
    });

    app.get('/icp', function(req, res, next) {
        res.render('pages/ideal-customer-profile', {showSuccess: false});
    });

    app.get('/leadgeneration', function(req, res, next) {
        res.render('pages/lead-generation', {});
    });

    app.get('/outboundsales', function(req, res, next) {
        res.render('pages/outbound-sales', {});
    });

	app.post('/icpsubmit', function (req, res, next) {
		var mail_text = '';
		if (req.body.form && (req.body.form === 'popup')){
			mail_text = 'first-name: ' + (req.body.firstName || "") + '\n' +
				'last-name: ' + (req.body.lastName || "") + '\n' +
				'company: ' + (req.body.company || "") + '\n' +
				'title: ' + (req.body.title || "") + '\n' +
				'email: ' + (req.body.email || "") + '\n' +
				'txtar1: ' + (req.body.txtar1 || "")
		} else {
			mail_text = 'first-name: ' + (req.body.firstName || "") + '\n' +
				'last-name: ' + (req.body.lastName || "") + '\n' +
				'company: ' + (req.body.company || "") + '\n' +
				'title: ' + (req.body.title || "") + '\n' +
				'email: ' + (req.body.email || "") + '\n' +
				'phone: ' + (req.body.phone || "")  + '\n' +
				'txtar1: ' + (req.body.txtar1 || "") + '\n' +
				'txtar2: ' + (req.body.txtar2 || "") + '\n' +
				'txtar3: ' + (req.body.txtar3 || "") + '\n' +
				'txtar4: ' + (req.body.txtar4 || "") + '\n' +
				'txtar5: ' + (req.body.txtar5 || "") + '\n' +
				'txtar6: ' + (req.body.txtar6 || "") + '\n' +
				'txtar7: ' + (req.body.txtar7 || "") + '\n' +
				'txtar8: ' + (req.body.txtar8 || "") + '\n' +
				'txtar9: ' + (req.body.txtar9 || "") + '\n' +
				'txtar10: ' + (req.body.txtar10 || "") + '\n' +
				'employee_size: ' + setArrayValue(req.body.employee_size) + '\n' +
				'company_revenue: ' + setArrayValue(req.body.company_revenue) + '\n' +
				'package: ' + (req.body.package || "") + '\n' +
				'price: ' + (req.body.price || "") + '\n' +
				'type: ' + (req.body.type || "")
		}

		try {
			var newQuestionnaire = new Questionnaire({
				firstName: (req.body.firstName || ""),
				lastName : (req.body.lastName || ""),

				company: (req.body.company || ""),
				title  : (req.body.title || ""),
				email  : (req.body.email || ""),
				phone  : (req.body.phone || ""),
				txtar1 : (req.body.txtar1 || ""),
				txtar2 : (req.body.txtar2 || ""),
				txtar3 : (req.body.txtar3 || ""),
				txtar4 : (req.body.txtar4 || ""),
				txtar5 : (req.body.txtar5 || ""),
				txtar6 : (req.body.txtar6 || ""),
				txtar7 : (req.body.txtar7 || ""),
				txtar8 : (req.body.txtar8 || ""),
				txtar9 : (req.body.txtar9 || ""),
				txtar10: (req.body.txtar10 || ""),

				employee_size: setArrayValue(req.body.employee_size),
				company_revenue: setArrayValue(req.body.company_revenue),

				package: (req.body.package || ""),
				price  : (req.body.price || ""),
				type   : (req.body.type || ""),

				date   : new Date()
			});

			newQuestionnaire.save();

			var message_result = sendMessage('yaro@upsail.io,nick@upsail.io', 'submission@upsail.io', 'Upsail Ideal Customer Profile', mail_text);

			if (message_result) {
				res.render('error', {
					message: 'Error while sending message',
					error  : {
						status: 500,
						stack : ''
					}
				});
			} else {
				res.render('pages/ideal-customer-profile', {showSuccess: true});
			}
		} catch (e) {
			eLogger.error('');
			res.render('error', {
				message: 'Error while sending message',
				error  : {
					status: 500,
					stack : ''
				}
			});
		}
	});

	app.get('/send_message', function(req, res, next) {
		var result = sendMessage('olexanr@ukr.net, alexholbik@gmail.com', 'instagramtestag@gmail.com', 'test subject', 'test message');

		if (result) {
			res.render('error', {
				message: 'Error while sending message',
				error  : {
					status: 500,
					stack : ''
				}
			});
		} else {
			res.render('pages/ideal-customer-profile', {showSuccess: true});
		}
	});
};

function setArrayValue(value){
	if (value) {
		if (Object.prototype.toString.call(value) === '[object Array]') {
			return value.join(', ');
		} else {
			return value;
		}
	} else {
		return '';
	}
}