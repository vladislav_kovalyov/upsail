var express = require('express');
var router = express.Router();

var Blog = require('../../models/blog').Blog;

var eLogger = require('../../lib/logger').eLogger;

var async = require('async');

var isLoggedInAdmin = require('../../lib/authenticated').isLoggedInAdmin;
router.use(isLoggedInAdmin);

// Storage
var multer = require('multer');
const storageBlog = multer.diskStorage({
    destination: function (req, file, callback) {
        callback(null, 'public/files/blog/');
    },
    filename   : function (req, file, callback) {
        var secs = new Date().getTime();
        callback(null, secs + '_' + file.originalname.replace(/\s/g, '_'));
    }
});

router.get('/', function(req, res) {
    res.render('pages/admin/admin-blog-list', {
        user: req.user
    });
});

router.get('/list', function(req, res) {
    Blog.find({})
        .exec(function (err, posts) {
            if (!err) {
                var list = {
                    data: []
                };

                posts.forEach(function (post) {
                    list.data.push([
                        post.blog_id,
                        post.title,
                        //post.priority,
                        post.blog_id
                    ]);
                });

                res.send(list);
            } else {
                eLogger.error(err);
                res.send({
                    list: []
                });
            }
        });
});

router.get('/new', function(req, res) {
    res.render('pages/admin/admin-blog-new', {
        user: req.user
    });
});

router.post('/new', function(req, res) {
    var uploadImage = multer({storage: storageBlog}).fields([
        {name: 'picture', maxCount: 1}
    ]);

    uploadImage(req, res, function (err) {
        if (err) {
            eLogger.error(err);
            res.render('pages/admin/admin-blog-new', {
                user    : req.user,
                message : 'Error save image'
            });
        } else {
            var newPost = new Blog({
                title: req.body.title,
                author: req.body.author,
                picture: '',
                description: req.body.description.substring(0, 125) + '\n' + req.body.description.substring(125, 250) + '...',
                dateFormatted: new Date().toString().split(' ')[1] + ' '
                    + new Date().toString().split(' ')[2] + ', '
                    + new Date().toString().split(' ')[3],
                text: req.body.text
            });

            if (req.files.picture) {
                var newPath = req.files.picture[0].path;
                newPath = newPath.split('\\').join('/');
                newPost.picture = newPath.replace('public', '');
            }

            newPost.save(function (err, item) {
                if (err) {
                    eLogger.error(err);
                    res.render('pages/admin/admin-blog-new', {
                        user    : req.user,
                        message : 'Error save post'
                    });
                } else {
                    res.redirect('/admin/blog');
                }
            });
        }
    });
});

router.get('/:id', function(req, res) {
    Blog.findOne({blog_id: req.params.id})
        .exec(function (err, post) {
            if (!err && post) {
                res.render('pages/admin/admin-blog-edit', {
                    user: req.user,
                    post: post
                });
            } else {
                eLogger.error(err);
                res.redirect('/admin/blog');
            }
    });
});

router.post('/:id/edit', function(req, res) {
    Blog.findOne({blog_id: req.params.id})
        .exec(function (err, post) {
            if (!err && post) {
                var uploadImage = multer({storage: storageBlog}).fields([
                    {name: 'picture', maxCount: 1}
                ]);

                uploadImage(req, res, function (err) {
                    if (err) {
                        eLogger.error(err);
                        res.render('pages/admin/admin-blog-edit', {
                            user    : req.user,
                            blog    : post,
                            message : 'Error save image'
                        });
                    } else {
                        post.title = req.body.title;
                        post.author = req.body.author;
                        post.description = req.body.description.substring(0, 125) + '\n' + req.body.description.substring(125, 250) + '...';
                        post.text = req.body.text;

                        if (req.files.picture) {
                            var newPath = req.files.picture[0].path;
                            newPath = newPath.split('\\').join('/');
                            post.picture = newPath.replace('public', '');
                        }

                        post.save(function (err, item) {
                            if (err) {
                                eLogger.error(err);
                                res.render('pages/admin/admin-blog-edit', {
                                    user    : req.user,
                                    blog    : post,
                                    message : 'Error save post'
                                });
                            } else {
                                res.redirect('/admin/blog');
                            }
                        });
                    }
                });
            } else {
                eLogger.error(err);
                res.redirect('/admin/blog');
            }
        });
});

router.get('/:id/delete', function(req, res) {
    Blog.findOne ({blog_id: req.params.id}, function (err, post) {
        if (!err && post) {
            post.remove (function (err, item) {
                if (err) {
                    eLogger.error (err);
                    res.send ({result: 0});
                } else {
                    res.send ({result: 1});
                }
            });
        }
    });
});

module.exports = router;