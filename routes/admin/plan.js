var express = require('express');
var router = express.Router();

var Plan = require('../../models/plan').Plan;

var eLogger = require('../../lib/logger').eLogger;

var isLoggedInAdmin = require('../../lib/authenticated').isLoggedInAdmin;
router.use(isLoggedInAdmin);

router.get('/', function(req, res) {
    res.render('pages/admin/admin-plan-list', {
        user: req.user
    });
});

router.get('/list', function(req, res) {
    Plan.find({})
        .exec(function (err, plans) {
            if (!err) {
                var list = {
                    data: []
                };

				plans.forEach(function (plan) {
					list.data.push([
						plan.plan_id,
						plan.name,
						plan.customQuote,
						plan.leadGrowth,
						plan.salesOpportunity,
						plan.salesBoost,
						null
					]);
				});

                res.send(list);
            } else {
                eLogger.error(err);
                res.send({
                    list: []
                });
            }
        });
});

router.get('/:id', function(req, res) {
    Plan.findOne({plan_id: req.params.id}, function (err, plan) {
		var message = req.session.err_message;
		req.session.err_message = null;

        if (!err && plan) {
            res.render('pages/admin/admin-plan-edit', {
                user : req.user,
                plan, message
            });
        } else {
            eLogger.error(err);
            res.redirect('/admin/plans');
        }
    });
});

router.post('/:id/edit', function(req, res) {
    Plan.findOne({plan_id: req.params.id}, function (err, plan) {
        if (!err && plan) {
			plan.name = req.body.name || '';
			plan.customQuote = req.body.customQuote || 0;
			plan.leadGrowth  = req.body.leadGrowth || 0;
			plan.salesOpportunity = req.body.salesOpportunity || 0;
			plan.salesBoost = req.body.salesBoost || 0;

            plan.save(function (err, item) {
                if (err) {
                    eLogger.error(err);
					req.session.err_message = 'Error saving plan';
					res.redirect('/admin/plans/' + req.params.id);
                } else {
                    res.redirect('/admin/plans');
                }
            });
        } else {
            eLogger.error(err);
			req.session.err_message = 'Error search plan';
			res.redirect('/admin/plans/' + req.params.id);
        }
    });
});

module.exports = router;