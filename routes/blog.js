var express = require('express');
var router = express.Router();

var Blog = require('../models/blog').Blog;

var eLogger = require('../lib/logger').eLogger;

var async = require('async');

router.get('/', function(req, res, next) {
    async.parallel([
        function (callback) {
            Blog.find({})
                .sort('-blog_id')
                .limit(5)
                .exec(function (err, posts) {
                    if (!err && posts) {
                        callback(null, posts);
                    } else {
                        callback(err, null);
                    }
                });
        },
        function (callback) {
            Blog.find({})
                .sort('-views')
                .limit(4)
                .exec(function (err, popularPosts) {
                    if (!err && popularPosts) {
                        callback(null, popularPosts);
                    } else {
                        callback(err, null);
                    }
                });
        },
        function (callback) {
            Blog.count()
                .exec(function (err, amount) {
                    if (!err && amount) {
                        callback(null, amount);
                    } else {
                        callback(err, null);
                    }
                });
        }
    ], function (err, results) {
        if (err) {
            eLogger.error(err);
            res.render('error', {
                message: 'Error while getting posts',
                error  : {
                    status: 500,
                    stack : ''
                }
            });
        } else {
            res.render('pages/blog', {
                posts       : results[0],
                popularPosts: results[1],
                amount      : results[2]
            });
        }
    });
});

router.get('/:id', function(req, res, next) {
    async.parallel([
        function (callback) {
            Blog.findOne({blog_id: req.params.id})
                .exec(function (err, post) {
                    if (!err && post) {
                        callback(null, post);
                    } else {
                        callback(err, null);
                    }
                });
        },
        function (callback) {
            Blog.find({})
                .sort('-views')
                .limit(4)
                .exec(function (err, popularPosts) {
                    if (!err && popularPosts) {
                        callback(null, popularPosts);
                    } else {
                        callback(err, null);
                    }
                });
        }
    ], function (err, results) {
        if (err) {
            eLogger.error(err);
            res.render('error', {
                message: 'Error while getting post',
                error  : {
                    status: 500,
                    stack : ''
                }
            });
        } else {
            results[0].views++;

            results[0].save(function (err, item) {
                if (err) {
                    eLogger.error(err);
                    res.render('error', {
                        message: 'Error while counting views',
                        error  : {
                            status: 500,
                            stack : ''
                        }
                    });
                } else {
                    res.render('pages/blog-page', {
                        post        : results[0],
                        popularPosts: results[1]
                    });
                }
            });
        }
    });
});

router.get('/getPost/:position/:id', function(req, res, next) {
    if (req.params.position === 'after') {
        Blog.find ({blog_id: {'$lt': req.params.id}})
            .sort ('-blog_id')
            .limit (1)
            .exec(function (err, post) {
                if (post[0]) {
                    res.send({result: true, blog_id: post[0].blog_id, isLastItem: false});
                } else {
                    res.send({result: true, message: "This is last post", isLastItem: true});
                }
            });
    } else {
        Blog.find ({blog_id: {'$gt': req.params.id}})
            .sort ('blog_id')
            .limit (1)
            .exec(function (err, post) {
                if (post[0]) {
                    res.send({result: true, blog_id: post[0].blog_id, isLastItem: false});
                } else {
                    res.send({result: true, message: "This is first post", isLastItem: true});
                }
            });
    }
});

router.get('/see/more', function(req, res, next) {
    Blog.find ({})
        .sort ('-blog_id')
        .skip(parseInt(req.query.items, 10))
        .limit (5)
        .exec(function (err, posts) {
            if (!err && posts) {
                res.send({result: 1, posts: posts});
            } else {
                eLogger.error(err);
                res.send({result: 0});
            }
        });
});

router.get('/search/posts', function(req, res, next) {
    var regexNameString = req.query.text.trim().split(' ').join('|');

    var reTitle = new RegExp('.*', 'ig');
    if (regexNameString !== '') {
        reTitle = new RegExp(regexNameString, 'ig');
    }

    if (req.query.text.length > 0) {
        Blog.find ({title : reTitle})
            .sort ('-blog_id')
            .exec(function (err, posts) {
                if (!err && posts) {
                    res.send({result: 1, posts: posts});
                } else {
                    eLogger.error(err);
                    res.send({result: 0});
                }
            });
    } else {
        Blog.find ({})
            .sort ('-blog_id')
            .limit (5)
            .exec(function (err, posts) {
                if (!err && posts) {
                    res.send({result: 1, posts: posts});
                } else {
                    eLogger.error(err);
                    res.send({result: 0});
                }
            });
    }
});

module.exports = router;