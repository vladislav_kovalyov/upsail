/**
 * Created by Alexandr_G on 06.12.2016.
 */
const mongoose = require('mongoose');
const config = require('./config');

// LOGGER =====================================
const eLogger = require('../lib/logger').eLogger;

mongoose.connect(config.get('mongoose:uri'));
const db = mongoose.connection;

const autoIncrement = require('mongoose-auto-increment-fix');
autoIncrement.initialize(mongoose.connection);

// When successfully connected
db.on('connected', function() {
    console.log('Mongoose default connection open');
});

// If the connection throws an error
db.on('error', function(err) {
    eLogger.error(err);
    console.log('Mongoose default connection error: ' + err);
});

// When the connection is disconnected
db.on('disconnected', function() {
    mongoose.disconnect();
    console.log('Mongoose default connection disconnected');
});

module.exports = mongoose;
module.exports.autoIncrement = autoIncrement;